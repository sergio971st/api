# -*- coding: utf-8 -*-
import datetime
from src.models import *


def fill_db(session):

    # Добавляем пользователей
    user_with_order = User(
        surname='User1', fathers_name='FNUser1', email='example1@ex.com')
    session.add(user_with_order)
    session.add(User(surname='User2', fathers_name='FNUser2',
                     email='example2@ex.com'))
    session.add(User(surname='User3', fathers_name='FNUser3',
                     email='example3@ex.com'))
    session.add(User(surname='User4', fathers_name='FNUser4',
                     email='example4@ex.com'))
    session.add(User(surname='User5', fathers_name='FNUser5',
                     email='example5@ex.com'))
    session.add(User(surname='User6', fathers_name='FNUser6',
                     email='example6@ex.com'))

    # Добавляем книги
    book1 = Book(name='Book1', author='Author1', isbn='isbn1')
    book2 = Book(name='Book2', author='Author2', isbn='isbn2')
    session.add(book1)
    session.add(book2)
    session.add(Book(name='Book3', author='Author3', isbn='isbn3'))
    session.add(Book(name='Book4', author='Author4', isbn='isbn4'))
    session.add(Book(name='Book5', author='Author5', isbn='isbn5'))

    # Добавляем магазины
    shop = Shop(name='Shop1', address='Address1', post_code='123')
    shop.books.append(book1)
    shop.books.append(book2)
    session.add(shop)
    session.add(Shop(name='Shop2', address='Address2', post_code='123'))
    session.add(Shop(name='Shop3', address='Address3', post_code='123'))

    # Добавляем заказ
    order = Order(reg_date=datetime.datetime.utcnow().date())
    user_with_order.orders.append(order)
    session.add(order)

    # Добавляем айтемы в заказ
    orderitem1 = OrderItem(book_quantity=3)
    orderitem2 = OrderItem(book_quantity=1)
    book1.order_items.append(orderitem1)
    book2.order_items.append(orderitem2)
    shop.order_items.append(orderitem1)
    shop.order_items.append(orderitem2)
    order.order_items.append(orderitem1)
    order.order_items.append(orderitem2)

    session.commit()
    return
