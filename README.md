# API

### USAGE:
1. Venv + установка зависимостей:
```
virtualenv -p python3.6 venv
. venv/bin/activate
pip install -r requirements.txt
cp settings.py.default settings.py
```
2. Локальный запуск:
```
python manage.py run (Пример демонстрационный, поэтому при каждом следующем запуске происходит перезапись данных)
```

