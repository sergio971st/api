# -*- coding: utf-8 -*-
import click

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import scoped_session, sessionmaker

from src.api import API
from fill_db import fill_db
from settings import DATABASE
from src.models import *


@click.group()
def cli():
    pass


@cli.command()
def run():
    DB_URI = URL(**DATABASE)
    Session = sessionmaker(autocommit=False,
                           autoflush=False,
                           bind=create_engine(DB_URI))
    session = scoped_session(Session)
    engine = create_engine(DB_URI)
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    fill_db(session)

    api = API(session)
    api.run()


if __name__ == '__main__':
    cli()
