# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, String, ForeignKey, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    surname = Column(String(50))
    fathers_name = Column(String(50))
    email = Column(String(50))
    orders = relationship('Order')

    def __init__(self, surname, fathers_name, email):
        self.surname = surname
        self.fathers_name = fathers_name
        self.email = email

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        to_serialize = ['id', 'surname', 'fathers_name', 'email']
        d = {}
        for attr_name in to_serialize:
            d[attr_name] = getattr(self, attr_name)
        return d


class Book(Base):
    __tablename__ = 'book'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    author = Column(String(50))
    isbn = Column(String(50))
    order_items = relationship('OrderItem')
    shop_id = Column(Integer, ForeignKey('shop.id'))

    def __init__(self, name, author, isbn):
        self.name = name
        self.author = author
        self.isbn = isbn

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        to_serialize = ['id', 'name', 'author', 'isbn']
        d = {}
        for attr_name in to_serialize:
            d[attr_name] = getattr(self, attr_name)
        return d


class Shop(Base):
    __tablename__ = 'shop'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    address = Column(String(50))
    post_code = Column(Integer)
    order_items = relationship('OrderItem')
    books = relationship('Book')

    def __init__(self, name, address, post_code):
        self.name = name
        self.address = address
        self.isbn = post_code

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        to_serialize = ['id', 'name', 'address', 'post_code']
        d = {}
        for attr_name in to_serialize:
            d[attr_name] = getattr(self, attr_name)
        return d


class Order(Base):
    __tablename__ = 'order'
    id = Column(Integer, primary_key=True)
    reg_date = Column(Date)
    user_id = Column(Integer, ForeignKey('user.id'))
    order_items = relationship('OrderItem')

    def __init__(self, reg_date):
        self.reg_date = reg_date

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        to_serialize = ['id', 'user_id']
        d = {
            'reg_date': self.reg_date.isoformat()
        }
        for attr_name in to_serialize:
            d[attr_name] = getattr(self, attr_name)
            d['reg_date'] = self.reg_date.isoformat()
        return d


class OrderItem(Base):
    __tablename__ = 'orderitem'
    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey('order.id'))
    book_id = Column(Integer, ForeignKey('book.id'))
    book_quantity = Column(Integer)
    shop_id = Column(Integer, ForeignKey('shop.id'))

    def __init__(self, book_quantity):
        self.book_quantity = book_quantity

    @classmethod
    def from_json(cls, data):
        return cls(**data)

    def to_json(self):
        to_serialize = ['id', 'order_id',
                        'book_id', 'book_quantity', 'shop_id']
        d = {}
        for attr_name in to_serialize:
            d[attr_name] = getattr(self, attr_name)
        return d
