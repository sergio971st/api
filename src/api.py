# -*- coding: utf-8 -*-
import datetime
import json
from aiohttp import web

from src.models import *
import settings


class API:

    def __init__(self, db_session, host=settings.HOST, port=settings.PORT):
        self.db_session = db_session
        self.port = port
        self.host = host

    async def user_info(self, request):
        instance_id = request.rel_url.query['id']
        instance = self.db_session.query(User).filter(
            User.id == instance_id).first()
        if instance:
            response_obj = instance.to_json()
            return web.Response(text=json.dumps(response_obj))
        else:
            return web.Response(text=json.dumps('No such item'))

    async def user_orders_info(self, request):
        instance_id = request.rel_url.query['id']
        instance = self.db_session.query(Order).filter(
            Order.user_id == instance_id).first()
        response_obj = instance.to_json()
        return web.Response(text=json.dumps(response_obj))

    async def books_in_the_shop(self, request):
        instance_id = request.rel_url.query['id']
        instances = self.db_session.query(
            Book).filter(Book.shop_id == instance_id)
        response_obj = []
        for instance in instances:
            response_obj.append(instance.to_json())
        return web.Response(text=json.dumps(response_obj))

    async def new_order(self, request):

        raw_books = str(request.rel_url)
        raw_books = raw_books[raw_books.find('?') + 1:]
        raw_books = raw_books.split('&')
        books = []
        for book in raw_books:
            books.append(tuple(int(item) for item in book.split('=')))

        # На данном этапе данные выглядят так: [('book_id', 'number_of_units'), ]
        order = Order(reg_date=datetime.datetime.utcnow().date())
        user = self.db_session.query(User).filter(User.id == 2).first()
        user.orders.append(order)
        self.db_session.add(order)
        for item in books:
            book = self.db_session.query(Book).filter(
                Book.id == item[0]).first()
            if book:
                # Считаем, что данная книга есть только в одном магазине
                shop = self.db_session.query(Shop).filter(
                    Shop.id == book.shop_id).first()
                order_item = OrderItem(book_quantity=item[1])
                order.order_items.append(order_item)
                book.order_items.append(order_item)
                shop.order_items.append(order_item)
                self.db_session.commit()
            else:
                return web.Response(text=json.dumps('No such item'))
        return web.Response(text=json.dumps('Order successfully created'))

    def run(self):
        app = web.Application()
        app.add_routes([web.get('/user', self.user_info),
                        web.get('/user_orders', self.user_orders_info),
                        web.get('/books_in_the_shop', self.books_in_the_shop),
                        web.post('/new_order', self.new_order), ])
        web.run_app(app)
